<?php

/**
 * @file
 * Hooks specific to the commerce_events_seats module.
 */

/**
 * Alter the add to cart form.
 *
 * @param array $data
 *   Metadata. With indexs room and form_state.
 * @param array $amountForm
 *   The amount form.
 * @param array $form
 *   The complete add to cart form.
 */
function hook_selectable_addtocart_alter(array $data, array &$amountForm, array &$form) {
  $amountForm['bloc1'] = [
    '#type' => 'number',
    '#title' => t('Bloc 1'),
    '#min' => 0,
  ];
}

/**
 * Allow alter the order item before add it to cart.
 *
 * @param OrderItemInterface $orderItem
 *   The order item.
 * @param array $context
 *   The room context.
 */
function hook_event_seats_add_to_car_alter(OrderItemInterface $orderItem, array $context) {
  $price = $context['seat']['bloc']['price'];
  $currency = $context['seat']['bloc']['currency'];
  $orderItem->setUnitPrice(new Price($price, $currency), TRUE);
  $orderItem->save();
}

/**
 * @} End of "addtogroup commerce_events_seats".
 */
