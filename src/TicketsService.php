<?php

namespace Drupal\commerce_events_seats;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Class TicketsService.
 */
class TicketsService implements TicketsServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new TicketsService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getOcupation(ProductVariationInterface $productVariation, $seatsFieldName, $pendingReserveMinutes = 10) {
    $resp = [];
    if ($productVariation && $productVariation->{$seatsFieldName} && $productVariation->{$seatsFieldName}->first()) {
      $room = Json::decode($productVariation->{$seatsFieldName}->first()->value);
      if (!isset($room['room']['blocs'])) {
        return $resp;
      }
      foreach ($room['room']['blocs'] as $bloc) {
        $query = $this->entityTypeManager->getStorage('commerce_event_ticket')->getQuery();
        $query->accessCheck(FALSE);
        $query->condition('product_variation', $productVariation->id());
        $query->condition('bloc', str_replace("-", "", $bloc['uuid']));

        $or = $query->orConditionGroup();
        $or->condition('state', 'confirmed');

        $and = $query->andConditionGroup();
        $and->condition('state', 'pending');
        $and->condition('created', (time() - ($pendingReserveMinutes * 60)), '>=');

        $or->condition($and);
        $query->condition($or);
        switch ($bloc['type']) {
          case 'unnumeredPluginToolbar':
            $result = $query->count()->execute();
            $resp[$bloc['uuid']] = $result;
            break;

          case 'numeredPluginToolbar':
            $result = $query->execute();
            $tickets = $this->entityTypeManager->getStorage('commerce_event_ticket')->loadMultiple($result);
            $resp[$bloc['uuid']] = [];
            foreach ($tickets as $ticket) {
              $resp[$bloc['uuid']][] = $ticket->seat->first()->value;
            }
            unset($tickets);
            unset($result);
            break;
        }
      }
    }

    return $resp;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlocOcupation(array $bloc, $pendingReserveMinutes = 10) {
    $resp = [];
    $query = $this->entityTypeManager->getStorage('commerce_event_ticket')->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('bloc', str_replace("-", "", $bloc['uuid']));

    $or = $query->orConditionGroup();
    $or->condition('state', 'confirmed');

    $and = $query->andConditionGroup();
    $and->condition('state', 'pending');
    $and->condition('created', (time() - ($pendingReserveMinutes * 60)), '>=');

    $or->condition($and);
    $query->condition($or);
    switch ($bloc['type']) {
      case 'unnumeredPluginToolbar':
        $result = $query->count()->execute();
        $resp[$bloc['uuid']] = $result;
        break;

      case 'numeredPluginToolbar':
        $result = $query->execute();
        $tickets = $this->entityTypeManager->getStorage('commerce_event_ticket')->loadMultiple($result);
        $resp[$bloc['uuid']] = [];
        foreach ($tickets as $ticket) {
          $resp[$bloc['uuid']][] = $ticket->seat->first()->value;
        }
        unset($tickets);
        unset($result);
        break;
    }

    return $resp;
  }

}
