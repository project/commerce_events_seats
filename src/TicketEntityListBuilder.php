<?php

namespace Drupal\commerce_events_seats;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Ticket entities.
 *
 * @ingroup commerce_events_seats
 */
class TicketEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('ID');
    $header['booked'] = $this->t('Booked');
    $header['product_variation'] = $this->t('Product variation');
    $header['bloc'] = $this->t('Bloc');
    $header['seat'] = $this->t('Seat');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_events_seats\Entity\TicketEntity $entity */
    $row['name'] = Link::createFromRoute(
      $entity->uuid(),
      'entity.commerce_event_ticket.canonical',
      ['commerce_event_ticket' => $entity->id()]
    );
    $row['booked'] = \Drupal::service('date.formatter')->format($entity->getBookedTime(), 'short');
    /** @var \Drupal\commerce_product\Entity\ProductVariation $variation */
    $variation = $entity->getProductVariation();
    if ($variation) {
      $product = $variation->getProduct();
      $row['product_variation'] = Link::createFromRoute(
        $product->label() . ' / ' . $variation->label(),
        'entity.commerce_product.canonical',
        ['commerce_product' => $variation->getProduct()->id()]
      );
    }
    else {
      $row['product_variation'] = $this->t('Wrong reference');
    }
    $row['bloc'] = $entity->getBlocLabel();
    $row['seat'] = $entity->getSeat();
    $row['status'] = $entity->getStateLabel();
    $row['changed'] = \Drupal::service('date.formatter')->formatTimeDiffSince($entity->getChangedTime());
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
