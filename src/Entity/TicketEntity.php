<?php

namespace Drupal\commerce_events_seats\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Defines the Ticket entity.
 *
 * @ingroup commerce_events_seats
 *
 * @ContentEntityType(
 *   id = "commerce_event_ticket",
 *   label = @Translation("Ticket"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_events_seats\TicketEntityListBuilder",
 *     "views_data" = "Drupal\commerce_events_seats\Entity\TicketEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\commerce_events_seats\Form\TicketForm",
 *       "add" = "Drupal\commerce_events_seats\Form\TicketForm",
 *       "edit" = "Drupal\commerce_events_seats\Form\TicketForm",
 *       "delete" = "Drupal\commerce_events_seats\Form\TicketDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_events_seats\TicketHtmlRouteProvider",
 *     },
 *
 *     "access" = "Drupal\commerce_events_seats\TicketEntityAccessControlHandler",
 *   },
 *   base_table = "commerce_event_ticket",
 *   translatable = FALSE,
 *   admin_permission = "administer ticket entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/ticket/{commerce_event_ticket}",
 *     "add-form" = "/ticket/add",
 *     "edit-form" = "/ticket/{commerce_event_ticket}/edit",
 *     "delete-form" = "/ticket/{commerce_event_ticket}/delete",
 *     "collection" = "/admin/content/ticket",
 *   },
 *   field_ui_base_route = "commerce_event_ticket.settings"
 * )
 */
class TicketEntity extends ContentEntityBase implements TicketEntityInterface {

  use EntityChangedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getProductVariationId() {
    return $this->get('product_variation')->first()->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setProductVariationId($pvid) {
    $key = $this->getEntityType()->getKey('product_variation');
    $this->set($key, $pvid);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProductVariation() {
    return $this->get('product_variation')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setProductVariation(ProductVariation $productVariation) {
    $key = $this->getEntityType()->getKey('product_variation');
    $this->set($key, $productVariation);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSeat() {
    return $this->get('seat')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function setSeat($seat) {
    $this->set('seat', $seat);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBloc() {
    return $this->get('bloc')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function setBloc($bloc) {
    $this->set('bloc', $bloc);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBlocLabel() {
    return $this->get('bloc_label')->value ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function setBlocLabel($label) {
    $this->set('bloc_label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBookedTime() {
    return $this->get('booked')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBookedTime($timestamp) {
    $this->get('booked')->value = $timestamp;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->t('Ticket @uuid', ['@uuid' => $this->uuid()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getStateLabel() {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->get('state')->first();
    if ($state) {
      return $state->getLabel();
    }
    else {
      return '[default]';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStateId() {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->get('state')->first();
    if ($state) {
      return $state->getId();
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->first();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setDisplayConfigurable('view', TRUE);

    // Tickets.
    $fields['booked'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Booked'))
      ->setDescription(t('Time when this ticket has value.'))
      ->setRequired(TRUE)
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', TRUE);

    $fields['product_variation'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Variant'))
      ->setSetting('target_type', 'commerce_product_variation')
      ->setTranslatable(FALSE)
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['bloc'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bloc UUID'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['bloc_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bloc'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['seat'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Seat'))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('The ticket state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setSetting('workflow', 'ticket_workflow')
      ->setDefaultValue('pending')
      ->setDisplayOptions('view', [
        'label' => 'about',
        'type' => 'state_transition_form',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
