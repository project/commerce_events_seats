<?php

namespace Drupal\commerce_events_seats\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Provides an interface for defining Ticket entities.
 *
 * @ingroup commerce_events_seats
 */
interface TicketEntityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Get product variation ID.
   *
   * @return int
   *   Product variation ID.
   */
  public function getProductVariationId();

  /**
   * Set product variation ID.
   *
   * @param int $pvid
   *   The product variation ID.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setProductVariationId($pvid);

  /**
   * Get product variation.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariation|null
   *   The product variation.
   */
  public function getProductVariation();

  /**
   * Set product variation.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $productVariation
   *   The product variation.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setProductVariation(ProductVariation $productVariation);

  /**
   * Gets the Ticket seat id.
   *
   * @return string
   *   Seat ID of the Ticket. Empty string to unnumbered blocs.
   */
  public function getSeat();

  /**
   * Sets the Ticket seat.
   *
   * @param string $seat
   *   The Ticket seat ID.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setSeat($seat);

  /**
   * Gets the Ticket bloc id.
   *
   * @return string
   *   Bloc of the Ticket.
   */
  public function getBloc();

  /**
   * Sets the Ticket bloc.
   *
   * @param string $bloc
   *   The Ticket bloc ID.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setBloc($bloc);

  /**
   * Gets the Ticket bloc label.
   *
   * @return string
   *   Bloc of the Ticket.
   */
  public function getBlocLabel();

  /**
   * Sets the Ticket bloc label.
   *
   * @param string $label
   *   The Ticket bloc label.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setBlocLabel($label);

  /**
   * Gets the Ticket booked timestamp.
   *
   * @return int
   *   Booked timestamp of the Ticket.
   */
  public function getBookedTime();

  /**
   * Sets the Ticket booked timestamp.
   *
   * @param int $timestamp
   *   The Ticket booked timestamp.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setBookedTime($timestamp);

  /**
   * Gets the Ticket creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ticket.
   */
  public function getCreatedTime();

  /**
   * Sets the Ticket creation timestamp.
   *
   * @param int $timestamp
   *   The Ticket creation timestamp.
   *
   * @return \Drupal\commerce_events_seats\Entity\TicketEntityInterface
   *   The called Ticket entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get state label.
   *
   * @return string
   *   The state label.
   */
  public function getStateLabel();

  /**
   * Get state id.
   *
   * @return string
   *   The state Id.
   */
  public function getStateId();

  /**
   * Get state instance.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItem
   *   The state value.
   */
  public function getState();

}
