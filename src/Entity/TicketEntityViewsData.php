<?php

namespace Drupal\commerce_events_seats\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Ticket entities.
 */
class TicketEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
