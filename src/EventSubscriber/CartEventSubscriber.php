<?php

namespace Drupal\commerce_events_seats\EventSubscriber;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_cart\Event\CartOrderItemRemoveEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_order\Event\OrderEvent;

/**
 * Cart Event Subscriber.
 */
class CartEventSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   Cart manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(
    MessengerInterface $messenger,
    CartManagerInterface $cart_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->messenger = $messenger;
    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_EMPTY => [['emptyCart', 100]],
      CartEvents::CART_ORDER_ITEM_REMOVE => [['removeOrderItem', 100]],
      OrderEvents::ORDER_PREDELETE => [['preDeleteOrder', 100]],
    ];
  }

  /**
   * Cancel related tickets.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order paid event.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function preDeleteOrder(OrderEvent $event) {
    $oiIds = [];
    foreach ($event->getOrder()->order_items as $orderItemValue) {
      $oiIds[] = $orderItemValue->target_id;
    }
    $orderItems = $this->entityTypeManager->getStorage('commerce_order_item')->loadMultiple($oiIds);
    foreach ($orderItems as $orderItem) {
      if ($orderItem && $orderItem->bundle() == 'event_order_item') {
        if ($orderItem->field_ticket) {
          $ticket = $this->entityTypeManager->getStorage('commerce_event_ticket')->load($orderItem->field_ticket->first()->target_id);
          if ($ticket) {
            $ticket->state = 'canceled';
            $ticket->save();
          }
        }
      }
    }
  }

  /**
   * Cancel related tickets.
   *
   * @param \Drupal\commerce_cart\Event\CartOrderItemRemoveEvent $event
   *   The cart add event.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function removeOrderItem(CartOrderItemRemoveEvent $event) {
    $orderItem = $event->getOrderItem();
    if ($orderItem && $orderItem->bundle() == 'event_order_item') {
      if ($orderItem->field_ticket) {
        $ticket = $this->entityTypeManager->getStorage('commerce_event_ticket')->load($orderItem->field_ticket->first()->target_id);
        if ($ticket) {
          $ticket->state = 'canceled';
          $ticket->save();
        }
      }
    }
  }

}
