<?php

namespace Drupal\commerce_events_seats\EventSubscriber;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Cart Event Subscriber.
 */
class CartPaidEventSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   Cart manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(
    MessengerInterface $messenger,
    CartManagerInterface $cart_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->messenger = $messenger;
    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => 'onOrderPlace',
    ];
  }

  /**
   * Confirm related tickets.
   *
   * @param \Drupal\commerce_events_seats\EventSubscriber\WorkflowTransitionEvent $event
   *   The event data.
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    $oiIds = [];
    foreach ($event->getEntity()->order_items as $orderItemValue) {
      $oiIds[] = $orderItemValue->target_id;
    }
    $orderItems = $this->entityTypeManager->getStorage('commerce_order_item')->loadMultiple($oiIds);
    foreach ($orderItems as $orderItem) {
      if ($orderItem && $orderItem->bundle() == 'event_order_item') {
        if ($orderItem->field_ticket) {
          $ticket = $this->entityTypeManager->getStorage('commerce_event_ticket')->load($orderItem->field_ticket->first()->target_id);
          if ($ticket) {
            $ticket->state = 'confirmed';
            $ticket->save();
          }
        }
      }
    }
  }

}
