<?php

namespace Drupal\commerce_events_seats\Form\Exception;

/**
 * Exception thrown when a the order item don't have the required fields.
 */
class OrderItemNotRequiredFieldException extends \InvalidArgumentException {
}
