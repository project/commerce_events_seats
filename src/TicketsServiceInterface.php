<?php

namespace Drupal\commerce_events_seats;

use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Interface TicketsServiceInterface.
 */
interface TicketsServiceInterface {

  /**
   * Get room ocupation.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariant
   *   The producto variant.
   * @param string $seatsFieldName
   *   The events seats field name.
   * @param int $pendingReserveMinutes
   *   Minutes to reserve pending tickets.
   *
   * @return array
   *   The ocupation array.
   */
  public function getOcupation(ProductVariationInterface $productVariant, $seatsFieldName, $pendingReserveMinutes = 10);

  /**
   * Get bloc ocupation.
   *
   * @param array $bloc
   *   The bloc definition.
   * @param int $pendingReserveMinutes
   *   Minutes to reserve pending tickets.
   *
   * @return array
   *   The ocupation array.
   */
  public function getBlocOcupation(array $bloc, $pendingReserveMinutes = 10);

}
