<?php

namespace Drupal\commerce_events_seats\Form;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_cart\Form\AddToCartForm;
use Drupal\Component\Serialization\Json;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\commerce_events_seats\Form\Exception\OrderItemNotRequiredFieldException;

/**
 * Provides the order item add to cart form.
 */
class SeatsAddToCartForm extends AddToCartForm {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHanlder;

  /**
   * Constructs a new AddToCartForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\commerce_order\Resolver\OrderTypeResolverInterface $order_type_resolver
   *   The order type resolver.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\commerce_price\Resolver\ChainPriceResolverInterface $chain_price_resolver
   *   The chain base price resolver.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    CartManagerInterface $cart_manager,
    CartProviderInterface $cart_provider,
    OrderTypeResolverInterface $order_type_resolver,
    CurrentStoreInterface $current_store,
    ChainPriceResolverInterface $chain_price_resolver,
    AccountInterface $current_user,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct(
      $entity_repository,
      $entity_type_bundle_info,
      $time, $cart_manager,
      $cart_provider,
      $order_type_resolver,
      $current_store,
      $chain_price_resolver,
      $current_user
    );
    $this->moduleHanlder = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('commerce_order.chain_order_type_resolver'),
      $container->get('commerce_store.current_store'),
      $container->get('commerce_price.chain_price_resolver'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If we do validation in validateForm() the event_seats_field  widget will
    // break. Then, if no selected seats we alert the user and not add anything
    // to the cart.
    $seats = $this->getSelectedSeats($form_state);
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->entity;
    /** @var \Drupal\commerce\PurchasableEntityInterface $purchased_entity */
    $purchased_entity = $order_item->getPurchasedEntity();

    $order_type_id = $this->orderTypeResolver->resolve($order_item);
    $store = $this->selectStore($purchased_entity);
    $cart = $this->cartProvider->getCart($order_type_id, $store);
    if (!$cart) {
      $cart = $this->cartProvider->createCart($order_type_id, $store);
    }

    // The order item must have this field's type.
    $ticketFieldName = $this->getOrderItemTicketField($order_item);
    // The Purchasable Entity must have this field's type.
    $bookedFieldName = $this->getOrderItemBookedField($purchased_entity);
    if (!empty($ticketFieldName) && !empty($bookedFieldName)) {
      // Generate tickets and add to cart.
      foreach ($seats as $seat) {
        /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
        $newOrderItem = $this->entity->createDuplicate();
        $newTicket = $this->entityTypeManager->getStorage('commerce_event_ticket')->create([
          'booked' => strtotime($purchased_entity->{$bookedFieldName}->first()->value),
          'product_variation' => $purchased_entity->id(),
          'bloc' => $seat['bloc_uuid'],
          'bloc_label' => $seat['bloc']['title'],
          'seat' => $seat['seat'],
          'state' => 'pending',
        ]);
        $newTicket->save();
        // Set the ticket in the order item.
        $newOrderItem->{$ticketFieldName} = $newTicket->id();
        $newOrderItem->save();
        // Allow other modules to alter the order item.
        $context = [
          'ticket_field_name' => $ticketFieldName,
          'booked_field_name' => $bookedFieldName,
          'seat' => $seat,
          'form_input' => $form_state->getUserInput(),
        ];
        $this->moduleHanlder->alter('event_seats_add_to_car', $newOrderItem, $context);
        // Add the order item to the cart.
        $this->entity = $this->cartManager->addOrderItem($cart, $newOrderItem, FALSE);
      }
    }
    else {
      // Alert to the site builder.
      throw new OrderItemNotRequiredFieldException('SeatsAddToCartForm requires a events_seats_field in the order item and a DateTime field in the purchased entity, product variantion.');
    }

    // Other submit handlers might need the cart ID.
    $form_state->set('cart_id', $cart->id());
  }

  /**
   * Get the first booked field in the order item.
   *
   * The booked field will be a datetime field.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $purchasableEntity
   *   The base order item.
   *
   * @return string|null
   *   The booked field name.
   */
  protected function getOrderItemBookedField(PurchasableEntityInterface $purchasableEntity) {
    $fields = $purchasableEntity->getFields();
    /** @var \Drupal\Core\Field\FieldItemList $field */
    foreach ($fields as $field) {
      /** @var \Drupal\Core\Field\BaseFieldDefinition $definition */
      $definition = $field->getFieldDefinition();
      // Ignore created & changed fields.
      if ($definition->getType() == 'datetime') {
        return $field->getName();
      }
    }
    return NULL;
  }

  /**
   * Get the first ticket field in the order item.
   *
   * The ticket field will be an entity_reference field referencing to
   * commerce_event_ticket entities.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $orderItem
   *   The base order item.
   *
   * @return string|null
   *   The ticket field name.
   */
  protected function getOrderItemTicketField(OrderItemInterface $orderItem) {
    $fields = $orderItem->getFields();
    /** @var \Drupal\Core\Field\FieldItemList $field */
    foreach ($fields as $field) {
      /** @var \Drupal\Core\Field\BaseFieldDefinition $definition */
      $definition = $field->getFieldDefinition();
      if ($definition->getType() == 'entity_reference') {
        $handler = $field->getSetting('handler');
        if (stripos($handler, ':commerce_event_ticket') !== FALSE) {
          return $field->getName();
        }
      }
    }
    return NULL;
  }

  /**
   * Get array with all selected seats, numbered or not.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Selected seats.
   */
  protected function getSelectedSeats(FormStateInterface $form_state) {
    $seats = [];
    $values = $form_state->getValues();
    $eventSeatFields = $form_state->get('event_seat_fields');

    foreach ($eventSeatFields as $eventSeatField) {
      foreach ($values[$eventSeatField] as $esfValue) {
        $room = Json::decode($esfValue['value']);
        // Process numbered seats.
        $selecteds = isset($room['room']['selected']) ? $room['room']['selected'] : [];
        foreach ($selecteds as $selected) {
          $id = explode('_', $selected);
          $item = [
            'bloc_uuid' => $id[0],
            'seat' => $id[1] . '_' . $id[2],
            'bloc' => $this->getBlocByUuid($room['room']['blocs'], $id[0]),
          ];
          $seats[] = $item;
        }
        // Process unnumbered seats.
        foreach ($room['room']['blocs'] as $bloc) {
          if ($bloc['type'] == 'unnumeredPluginToolbar') {
            $bid = str_replace('-', '', $bloc['uuid']);
            for ($i = 0; $i < $values['bloc_' . $bid]; ++$i) {
              $item = [
                'bloc_uuid' => $bid,
                'seat' => '',
                'bloc' => $bloc,
              ];
              $seats[] = $item;
            }
          }
        }
      }
    }

    return $seats;
  }

  /**
   * Get the bloc array.
   *
   * @param array $blocs
   *   The bloc list.
   * @param string $uuid
   *   The required Uuid.
   *
   * @return array|null
   *   The bloc.
   */
  protected function getBlocByUuid(array $blocs, $uuid) {
    foreach ($blocs as $bloc) {
      if (str_replace('-', '', $bloc['uuid']) == $uuid) {
        return $bloc;
      }
    }
    return NULL;
  }

}
