<?php

namespace Drupal\commerce_events_seats\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ticket entities.
 *
 * @ingroup commerce_events_seats
 */
class TicketDeleteForm extends ContentEntityDeleteForm {


}
