/**
 * @file
 * Attaches the behaviors for the travel book details page.
 */

(function ($, Drupal, drupalSettings) {

  /**
   * Attaches the fieldUIOverview behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the fieldUIOverview behavior.
   *
   * @see Drupal.fieldUIOverview.attach
   */
  Drupal.behaviors.CommerceEventsSeatsDemo = {
    attach(context, settings) {
      $(function () {
        $('.room-template-editor').each(function (i, editor) {
          // Add seat status event handler.
          $("[data-uuid=\"" + $(editor).attr('data-uuid') + "\"]").once().on("eventSeatEditor:onSeatStatusChanging", function (event, data) {
            console.log('commerce_data', data);
            var selects = $('#numbered_edit_' + data.bloc + ' .selects ul');
            if (data.new_state == 'selected') {
              var seat = selects.find('#seat_extra_' + data.seat_id);
              if (seat.length == 0) {
                selects.append('<li id="seat_extra_' + data.seat_id + '"><span>' + data.row + '_' + data.column + '&nbsp;: </span><select name="seat_extra_' + data.seat_id + '"><option value="opcA">Option A</option><option value="opcB">Option B</option></select></li>');
              }
            }
            else {
              selects.find('#seat_extra_' + data.seat_id).remove();
            }
          });
        });

      });
    }
  };
})(jQuery, Drupal, drupalSettings);
